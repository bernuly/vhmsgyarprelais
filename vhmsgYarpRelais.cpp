
#include "vhcl.h"


#ifdef _WINDOWS
#include <windows.h>
#include <WinBase.h>
#include <mmsystem.h>
#include <conio.h>
#endif


#include <time.h>

#ifdef UNIX
#include <unistd.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <map>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iomanip>
#include <list>

#include "vhmsg-tt.h"


#include <yarp/os/Bottle.h>
#include <yarp/os/BufferedPort.h>
#include <yarp/os/Network.h>
#include <yarp/os/Port.h>
#include <yarp/os/Time.h>


using namespace vhmsg;
using namespace std;
using namespace yarp::os;

enum {
	PROC_SBM,
	PROC_BML,
	PROC_SNDSTIM
};

Network* yNetwork;
BufferedPort<Bottle> yPortInBML;
BufferedPort<Bottle> yPortInSBM;
BufferedPort<Bottle> yPortInSoundStimulus;
BufferedPort<Bottle> yPortOut;



list<string> lstActors;

class DataProcessor : public TypedReaderCallback<Bottle> {

public:
	DataProcessor(int _iType, string _strAgentName="") : iType(_iType), strAgentName(_strAgentName){ };

	virtual void onRead(Bottle& b) {
		// process data in b
#ifdef DEBUG
		if(bBML){
			cout << endl << "in BML: ";
		} else {
			cout << endl << "in SBM: ";
		}
		cout << b.toString() << endl;
#endif

		/* we need this because yarp tokenized the string into escaped elements... */
		string strBottle;
		while(b.size()>0){
			string t = b.pop().toString().c_str();
			strBottle = t + " " + strBottle;
		}


//		if(bBML){
		if(iType == PROC_BML){
			time_t tNow;
			tNow = time(NULL);
			int iSecs = tNow - floor(tNow / 1000) * 1000; //get the last 1000 seconds
			stringstream ss;
			ss << setfill('0');
			ss << setw(3) << iSecs;
			ss << setw(5) << rand();
			string strID = "bml_" + ss.str();

			string strMessage = strAgentName + " ALL " + strID + " " + strBottle;
#ifdef DEBUG
			cout << "sending message to SmartBody: " <<  strMessage << endl;
#endif

			ttu_notify2( "vrSpeak", strMessage.c_str() );
			cout << "sending " << strBottle << " to vrSpeak" << endl;
		} else if (iType == PROC_SBM) {
			ttu_notify2("sb", strBottle.c_str());
		}
		else if (iType == PROC_SNDSTIM) {
			ttu_notify2("soundStimuli", strBottle.c_str());
			cout << "sending " << strBottle << " to soundStimuli" << endl;
		}

	}

private:
	int iType;
	string strAgentName;

};

void tt_client_callback(const char * op, const char * args, void * ) {
	//cerr << "received " << op << ":" <<  args << endl;

	Bottle& bottleMain = yPortOut.prepare(); 
	bottleMain.clear();

	/* we parse the input into tokens, so that we can create a bottle with a list of Values */
	std::istringstream iss(args);
	std::string token;
	while(getline(iss, token, ';')) {
		//		cout << token << endl;	
		Bottle bottleTemp(token.c_str());
		bottleMain.addList() = bottleTemp;
	}

	//    bottleMain.addString(args);
	if(bottleMain.size() > 0){
		yPortOut.write();         
	}   
}


int main(int argc, char* argv[]) {


	map<std::string, std::string> mapConfig;
	string strActor;

	bool bPollSmartBody = FALSE;
	//    strActor = "utah";
	strActor = "ChrBrad";
	string strActiveMQServer = "127.0.0.1"; //"192.168.0.10";

	string strPortOut = "/SB/out"; 
	string strPortInBML = "/SB/in/bml";
	string strPortInSBM = "/SB/in/sbm";
	string strPortInSoundStimulus = "/SB/in/soundstimuli";

	//-----------------------
	std::string strConfigFile = "config.txt";
	std::ifstream ifInfile(strConfigFile);

	if(ifInfile.good()){
//		std::cout << "Found config file" << std::endl;
		std::string strLine;
		while( std::getline(ifInfile, strLine) ) {
			std::istringstream isLine(strLine);
			std::string strKey;
			if( std::getline(isLine, strKey, '=') ) {
				std::string strValue;
				if( std::getline(isLine, strValue) ) {
//					std::cout << "Key: " << strKey << ", strValue: " << strValue << std::endl;
					mapConfig[strKey] = strValue;
				}
			}
		}
		ifInfile.close();
		std::map<std::string, std::string>::iterator it;

		it = mapConfig.find("Polling");
		if (it != mapConfig.end()) {
			string strTemp = it->second;
			stringstream ss;
			ss << strTemp; 
			ss >> std::boolalpha >> bPollSmartBody; 
		}

		it=mapConfig.find("Actor");
		if(it!=mapConfig.end()){
			strActor = it->second;
		}

		it = mapConfig.find("Actors");
		if (it != mapConfig.end()) {
			string strActors = it->second;
			stringstream ss(strActors);
			string token;
			while (ss >> token) {
				lstActors.push_back(token);
			}
		}


		it=mapConfig.find("PortOutput");
		if(it!=mapConfig.end()){
			strPortOut = it->second;
		}

		it=mapConfig.find("PortBMLinput");
		if(it!=mapConfig.end()){
			strPortInBML = it->second;
		}

		it=mapConfig.find("PortPythonCmdInput");
		if(it!=mapConfig.end()){
			strPortInSBM = it->second;
		}
		it = mapConfig.find("activeMQServer");
		if (it != mapConfig.end()) {
			strActiveMQServer = it->second;
		}

	}


	cout << "---------------------------------" << endl;
	cout << " vhmsgYarpRelais" << endl;
	cout << "   compiled " << __DATE__ << ", " << __TIME__ << endl << endl;
	cout << "   poll SmartBody: " << std::boolalpha << bPollSmartBody << endl;
	cout << "   actor: " << strActor << endl;
	cout << "   activeMQServer: " << strActiveMQServer << endl;
	cout << "   YARP ports: " << endl;
	cout << "      output: " << strPortOut   << endl;
	cout << "      BML input: " << strPortInBML << endl;
	cout << "      python cmd input: " << strPortInSBM << endl;
	cout << "      Sound stimulus input: " << strPortInSoundStimulus << endl << endl;

	yNetwork = new Network();
	yPortOut.open(strPortOut.c_str()); 
	yPortInBML.open(strPortInBML.c_str());
	yPortInSBM.open(strPortInSBM.c_str());
	yPortInSoundStimulus.open(strPortInSoundStimulus.c_str());


	

	for (auto&& actr : lstActors) {
		string strPortName = "/SB/" + actr + "/in/bml";
//		cout << "strPortName: " << strPortName << endl;
		BufferedPort<Bottle> *yP = new BufferedPort<Bottle>();
		yP->open(strPortName.c_str());

		DataProcessor* processorBML=new DataProcessor(PROC_BML, actr);
		yP->useCallback(*processorBML);  // input should go to processor.onRead()
	}


	DataProcessor processorBML(PROC_BML, strActor);
	yPortInBML.useCallback(processorBML);  // input should go to processor.onRead()

	DataProcessor processorSBM(PROC_SBM);
	yPortInSBM.useCallback(processorSBM);  // input should go to processor.onRead()

	DataProcessor processorSoundStimulus(PROC_SNDSTIM);
	yPortInSoundStimulus.useCallback(processorSoundStimulus);  // input should go to processor.onRead()

	int err;

	ttu_set_client_callback( tt_client_callback );

	err = ttu_open( strActiveMQServer.c_str() );

	if ( err == TTU_ERROR ) {
		printf( "Connection error!\n" );
		return -1;
	}

	/* before we can to the polling, we need to setup some functions (python does not allow to send everything on a single line...) */
	ttu_notify2("sb", "button_touched = 0;");
	ttu_notify2("sb", "def xPosStr(charName): charTemp = scene.getCharacter(charName); return (charName + \"_x \" + str(charTemp.getPosition().getData(0)) + \";\");");
	ttu_notify2("sb", "def zPosStr(charName): charTemp = scene.getCharacter(charName); return (charName + \"_z \" + str(charTemp.getPosition().getData(2)) + \";\");");
	ttu_notify2("sb", "def oriPosStr(charName): charTemp = scene.getCharacter(charName); return (charName + \"_ori \" + str(charTemp.getOrientation().getData(0)) + \";\");");
	ttu_notify2("sb", "def charStr(charName): return(xPosStr(charName) + zPosStr(charName) + oriPosStr(charName));");


	string strPollMessage =
		string("strBT = \"button_touched \" + str(button_touched) + \";\";\n") + 
		string("mm = strBT;\n") +
		string("cc = scene.getNumCharacters();\n") +
		string("lst = scene.getCharacterNames();\n") +
		/* and then some python list magic because we cannot define the loop within a lambda function... */
		string("lst_out = [charStr(x) for x in lst];\n")+
		string("mm = mm + \'%s\' % \'; \'.join(map(str, lst_out));\n")+ 
		string("scene.vhmsg2(\"poll\", mm);");


	//cout << "strPollMessage: " << strPollMessage << endl;

	err = ttu_register( "poll" );
	/* FOR TESTING 
	err = ttu_register( "sb" );
	err = ttu_register( "sbm" );
	err = ttu_register( "vrAgentBML" );
	err = ttu_register( "vrExpress" );
	err = ttu_register( "vrSpeak" );
	err = ttu_register( "RemoteSpeechReply" );
	err = ttu_register( "PlaySound" );
	err = ttu_register( "StopSound" );
	err = ttu_register( "CommAPI" );
	err = ttu_register( "object-data" );
	err = ttu_register( "vrAllCall" );
	err = ttu_register( "vrKillComponent" );
	err = ttu_register( "receiver" );
	err = ttu_register( "sbmdebugger" );
	err = ttu_register( "vrPerception" );
	err = ttu_register( "vrBCFeedback" );
	err = ttu_register( "vrSpeech" );
	*/

	while (1) {
		err = ttu_poll();
		if( err == TTU_ERROR ) {
			printf( "ttu_poll ERR\n" );
		}

		
		if (bPollSmartBody) {
			ttu_notify2("sb", strPollMessage.c_str());
		}

		//	cout << "sending poll message" << endl;
		//	cerr << ".";
		//#define UNIX
#ifdef UNIX
		usleep(10e4);
#else
		Sleep(10e1);
#endif
	}


	ttu_close();
}



/* procedure:
yarp sending:
- attached to the the sb call back

yarp reading:
- in the main loop (we could check for yarp call backs...)

*/


