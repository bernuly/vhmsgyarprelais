mkdir vhmsgYarpRelais\bin
cd vhmsgYarpRelais/bin
copy ..\..\Release\vhmsgYarpRelais.exe .
copy ..\..\config.txt .
copy ..\..\activemq\activemq-cpp\vs2015-build\Win32\ReleaseDLL\activemq-cpp.dll
copy ..\..\pthreads\lib\pthreadVCE2.dll
copy ..\..\activemq\apr\apr\lib\libapr-1.dll
copy ..\..\activemq\apr\apr-util\lib\libaprutil-1.dll
copy ..\..\activemq\apr\apr-iconv\lib\libapriconv-1.dll
copy C:\src\yarp-git_VS15\bib\Release\YARP_dev.dll . 
copy C:\src\yarp-git_VS15\bin\Release\YARP_sig.dll .
copy C:\src\yarp-git_VS15\bin\Release\YARP_init.dll .
copy C:\src\yarp-git_VS15\bin\Release\YARP_OS.dll .
copy C:\src\ACE-6.3.3\lib\ACE.dll .


cd ..\..
REM echo %date% | sed s/-//g | awk '{print "vhmsgYarpRelais_"$1".zip"}' > tmp
for /f %%i in ('powershell get-date -format "{yyyMMdd}"') do set MYDATE=%%i
echo vhmsgYarpRelais_%MYDATE:/=%.zip > tmp
set /p filename=<tmp
set ZIP_EXE="C:\Program Files (x86)\GnuWin32\bin\zip.exe"
%ZIP_EXE% -r %filename% vhmsgYarpRelais/

REM rm -r vhmsgYarpRelais
REM rm tmp

